import arcpy
from arcpy import env
import os
from collections import Counter

env.workspace = "C:\\Users\\Will\\GitKraken\\SnapPointsWithAttributes\\TestData\\"
env.overwriteOutput = True

in_pts_bars = "nodes_bars_synth.shp"
in_pts_strms = "nodes_strms_synth.shp"  # these are assumed to already be snapped to the ends of in_lines
in_lines = "streams_synth.shp"

searchRadius = "100 Meters"

## string expressions for dynamic naming
p_pref, p_suff = str.split(in_pts_bars,".")
p_pref_str, p_suff_str = str.split(in_pts_strms,".")
l_pref, l_suff = str.split(in_lines,".")


#### make copies, snap, and split ####

## make copy of barrier points and snap(modifies pts in-place) to lines
pts_snap = "\\Outputs\\" + p_pref + "_snap." + p_suff
arcpy.CopyFeatures_management(in_pts_bars, pts_snap) # Execute Copy
print(pts_snap)
snapEnv = [in_lines, "EDGE", searchRadius]
arcpy.Snap_edit(pts_snap,[snapEnv]) # execute Snap

## make copy of stream points
str_pts_copy = "\\Outputs\\" + p_pref_str + "_copy." + p_suff_str
print(str_pts_copy)
arcpy.CopyFeatures_management(in_pts_strms, str_pts_copy) # Execute Copy

## split lines by barriers
lines_split = "\\Outputs\\" + l_pref + "_split." + l_suff
print(lines_split)
arcpy.SplitLineAtPoint_management(in_lines, pts_snap, lines_split, "1 Meters")

## delete duplicate lines
fields = ["Shape"] # Set the field upon which the identicals are found
xy_tol = "0.01 Meters" # Set the XY tolerance within which to identical records to be deleted
z_tol = "" # Set the Z tolerance to default
arcpy.DeleteIdentical_management(lines_split, fields, xy_tol, z_tol) # modifies input in-place


#### identify and remove lines without barriers ####

## find stream segments that have not been split
# get count of line segments sharing same ID values
orig_strID = Counter([row[0] for row in arcpy.da.SearchCursor(lines_split, "str_ID")])
# return only line segment IDs that are unshared 
d = dict((k,v) for k,v in orig_strID.items() if v < 2).keys()

## remove stream segments that lack barriers and save as separate fc
inFeatures = lines_split
tempLayer = "lines_split_temp"
outFeatures = "\\Outputs\\" + l_pref + "_splitonly." + l_suff  
expression = ' "str_ID" IN ({0})'.format(', '.join(map(str,d)))

try:
    arcpy.CopyFeatures_management(inFeatures, outFeatures)
 
    # Execute MakeFeatureLayer
    arcpy.MakeFeatureLayer_management(outFeatures, tempLayer)
 
    # Execute SelectLayerByAttribute to determine which features to delete
    arcpy.SelectLayerByAttribute_management(tempLayer, "NEW_SELECTION", expression)
 
    # Execute GetCount and if some features have been selected, then 
    #  execute DeleteFeatures to remove the selected features.
    if int(arcpy.GetCount_management(tempLayer).getOutput(0)) > 0:
        arcpy.DeleteFeatures_management(tempLayer)
         
except Exception:
    e = sys.exc_info()[1]
    arcpy.AddError(e.args[0])


#### attribute transefer and preparation of line segments as 'edges'

lines_split = outFeatures

## convert end vertices to points
line_ends = "\\Outputs\\" + l_pref + "_ends." + l_suff
arcpy.FeatureVerticesToPoints_management(lines_split, line_ends, "BOTH_ENDS")


## delete duplicate ends
fields = ["Shape"] # Set the field upon which the identicals are found
xy_tol = "0.01 Meters" # Set the XY tolerance within which to identical records to be deleted
z_tol = "" # Set the Z tolerance to default
arcpy.DeleteIdentical_management(line_ends, fields, xy_tol, z_tol) # modifies input in-place


## add geometry attribute fields for lines fc
arcpy.management.AddGeometryAttributes(lines_split, "LINE_START_MID_END")
arcpy.management.AddGeometryAttributes(lines_split, "LENGTH_GEODESIC")


## compute concatenated xy fields for line start and ends (for attribute-based joins with snapped barrier points)
# create new fields
fld_leng = 30
arcpy.AddField_management(lines_split, "XY_start", "TEXT", "", "", fld_leng)
arcpy.AddField_management(lines_split, "XY_end", "TEXT", "", "", fld_leng)

# concatenate start and end values to single filed (to be used for joining later)
with arcpy.da.UpdateCursor(lines_split, ["str_ID","START_X","START_Y","END_X","END_Y","XY_start","XY_end"]) as uCur:

    for row in uCur:
        print(row)
        str_ID, startX, startY, endX, endY, start, end = row[0], row[1], row[2], row[3], row[4], row[5], row[6]
        start = str(startX) + "_" + str(startY)
        end = str(endX) + "_" + str(endY)
        new = uCur.updateRow([str_ID, startX, startY, endX, endY, start, end])
        
        print("Lines: start and end coords concatenated")
del uCur

## add geometry fields for snapped barrier points
arcpy.management.AddGeometryAttributes(pts_snap, "POINT_X_Y_Z_M")

## compute concatenated xy fields for line start and ends (for attribute-based joins with snapped barrier points)
# create new fields
fld_leng = 30
arcpy.AddField_management(pts_snap, "XY_concat", "TEXT", "", "", fld_leng)

# concatenate start and end values to single field (to be used for joining later)
with arcpy.da.UpdateCursor(pts_snap, ["bar_ID", "POINT_X", "POINT_Y", "XY_concat"]) as uCur:

    for row in uCur:
        ID, ptX, ptY, XY = row[0], row[1], row[2], row[3]
        XY = str(ptX) + "_" + str(ptY)
        new = uCur.updateRow([ID, ptX, ptY, XY])
        
    print("Points: coords concatenated")
del uCur

## add geometry fields for stream points
arcpy.management.AddGeometryAttributes(str_pts_copy, "POINT_X_Y_Z_M")

## compute concatenated xy fields for line start and ends (for attribute-based joins with snapped barrier points)
# create new fields
fld_leng = 30
arcpy.AddField_management(str_pts_copy, "XY_concat", "TEXT", "", "", fld_leng)

# concatenate start and end values to single field (to be used for joining later)
with arcpy.da.UpdateCursor(str_pts_copy, ["str_ID", "POINT_X", "POINT_Y", "XY_concat"]) as uCur:

    for row in uCur:
        ID, ptX, ptY, XY = row[0], row[1], row[2], row[3]
        XY = str(ptX) + "_" + str(ptY)
        new = uCur.updateRow([ID, ptX, ptY, XY])
        
    print("Points: coords concatenated")
del uCur

## add nodeIDs to lines fc for start and end of each line segment

# create new attribute fields 
fld_leng = 10
arcpy.AddField_management(lines_split, "nodID_strt", "LONG", "", "", fld_leng)
arcpy.AddField_management(lines_split, "nodID_end", "LONG", "", "", fld_leng)


# define local vars
update_fc = lines_split  #feature class to update
updateFld_strt = "nodID_strt"  # update field to be populated
IdFld_strt = "XY_start"  # key field for populating 'start' node IDs
updateFld_end = "nodID_end"  # update field to be populated
IdFld_end = "XY_end"  # key field for populating 'end' node IDs

join_fc_bar = pts_snap  #join feature class
join_fc_str = str_pts_copy  #join feature class
joinValFld_bar = "bar_ID"  #join value field to be transferred
joinValFld_str = "str_ID"  #join value field to be transferred
joinIdFld = "XY_concat"  #join key field


#create a python dict dictionary from barrier points
#Key: join field
#Value: bar_ID field
bar_dict = dict([(key, val) for key, val in arcpy.da.SearchCursor(join_fc_bar, [joinIdFld, joinValFld_bar])])
print(bar_dict)

#create a python dict dictionary from strm points
#Key: join field
#Value: str_ID field
str_dict = dict([(key, val) for key, val in arcpy.da.SearchCursor(join_fc_str, [joinIdFld, joinValFld_str])])
print(str_dict)

# merge the two dicts
value_dict = dict(str_dict.items() + bar_dict.items())

##value_dict = bar_dict.update(str_dict)

for k,v in value_dict.items():
    print(k,v)

# use dict and update cursor to populate start node IDs in lines fc
with arcpy.da.UpdateCursor (update_fc, [updateFld_strt, IdFld_strt]) as uCur:
    for update, key in uCur:
        #skip if key value is not in dictionary
        if not key in value_dict:
            continue
        #create row tuple
        row = (value_dict[key], key)

        #update row
        uCur.updateRow(row)
del uCur

# use dict and update cursor to populate end node IDs in lines fc
updateFld = "nodID_end"  #update field
IdFld = "XY_end"  #update fc key field

with arcpy.da.UpdateCursor (update_fc, [updateFld_end, IdFld_end]) as uCur:
    for update, key in uCur:
        #skip if key value is not in dictionary
        if not key in value_dict:
            continue
        #create row tuple
        row = (value_dict[key], key)

        #update row
        uCur.updateRow(row)
del uCur


#### cleanup

## remove unneeded fcs
arcpy.Delete_management(pts_snap)
arcpy.Delete_management(str_pts_copy)

## remove unneeded fields
# modified from https://gis.stackexchange.com/a/236785/17482
FCfields = [f.name for f in arcpy.ListFields(lines_split)]
KeepFields = ['Shape', 'FID', 'str_ID', 'LENGTH_GEO', 'XY_start', 'XY_end', 'nodID_strt', 'nodID_end']
fields2Delete = list(set(FCfields) - set(KeepFields))
##fields2Delete = ['MID_X', 'END_X','START_X', 'END_Y', 'MID_Y', 'START_Y']
arcpy.DeleteField_management(lines_split, fields2Delete)
